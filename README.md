Introduction
------------

Testiphy is a cross-project likelihood testsuite for phylogenetic estimation
programs.  Testiphy provides standard data sets for comparing
likelihoods across programs. Testiphy aims to help software developers
* correctly implement complex substitution models
* implement CI testing
* provide likelihood unit tests for a variety of programs

Each unit test consists of an aligned data set, a tree, and a set of
parameter values.

Testiphy does not currently test the ability of programs to maximize
the likelihood, but tests the likelihood of specific parameter values.

Contributions of new tests, new code, and drivers for new programs are welcome!  Join the [testiphy](https://groups.google.com/forum/#!forum/testiphy) mailing list or browse the archives to see what's going on.

Programs
--------

Testiphy currently has some coverage for BAli-Phy, RevBayes, PAUP, raxml-ng, IQ-TREE, and PhyML.

The goal is to add tests for PAML, BEAST1, BEAST2, phylobayes, and
other packages that compute phylogenetic likelihoods.

Test coverage
-------------

Testiphy currently has tests for
* JC
* GTR+G4+I
* amino acid models
  * WAG
  * LG08
* codon models
  * Goldman-Yang '94
  * Muse-Gaut '94
  * M3
  * F1x4 and F3x4
  * branch-site

A few tests have theoretical results.

Install
-------

```
git clone git@gitlab.com:testiphy/testiphy.git
```

For instructions on installing the software being tested, follow the links to each software package below.

Usage
-----

You need to run the comand in the `testiphy/` directory that you created when you cloned testiphy:
```
cd testiphy
./testiphy <program name>
```
If you run testiphy in a subdirectory (i.e. `testiphy/tests/likelihood/codons`) then it will run only the codon tests.
If you run testiphy in subdirectories, you might want to add the `testiphy/` directory to your `$PATH`.

Software | Command
---------|---------
[HyPhy](https://veg.github.io/hyphy-site/) ([source](https://github.com/veg/hyphy))| ./testiphy hyphymp
[raxml-ng](https://github.com/amkozlov/raxml-ng/releases) ([source](https://github.com/amkozlov/raxml-ng)) | ./testiphy raxml-ng
[IQ-Tree](http://www.iqtree.org/)  ([source](https://github.com/Cibiv/IQ-TREE)) | ./testiphy iqtree
[PhyML](http://www.atgc-montpellier.fr/phyml/binaries.php) ([source](https://github.com/stephaneguindon/phyml))| ./testiphy phyml
[PAUP*](https://paup.phylosolutions.com/) (no source) | ./testiphy paup
[revbayes](https://revbayes.github.io/) ([source](https://github.com/revbayes/revbayes)) | ./testiphy rb
[bali-phy](http://www.bali-phy.org) ([source](https://github.com/bredelings/BAli-Phy)) | ./testiphy bali-phy

Integrating into CI
-------------------

You can add testiphy to your existing `.travis.yml` file like this:
```
script:
# build and install software into $HOME/local
  - ...
# put the installed binary into the $PATH
  - export PATH=$HOME/local/bin:$PATH
# install testiphy
  - cd
  - git clone https://gitlab.com/testiphy/testiphy.git
# run testiphy
  - cd testiphy
  - ./testiphy <software name>
```

See example `.travis.yml` files for:
* [raxml-ng](https://github.com/amkozlov/raxml-ng/blob/master/.travis.yml)
* [revbayes](https://github.com/revbayes/revbayes/blob/development/.travis.yml)
* [bali-phy](https://github.com/bredelings/BAli-Phy/blob/master/.travis.yml)

If you want to temporarily disable an individual test, you can add a line to delete that directory:
```
...
  - cd testiphy
  - rm -rf tests/likelihood/3   # disable test `likelihood/3`
  - ./testiphy <software name>
```

Related projects and resources
------------------------------

* Alex Griffing's [phyly](https://github.com/argriffing/phyly) project for computing likelihoods to arbitrary precision with error bounds.
* Rob Lanfear's [BenchmarkAlignments](https://github.com/roblanf/BenchmarkAlignments/) on github.
* Alexis Stamatakis's [test-Datasets](https://github.com/stamatak/test-Datasets) on github.
