import           Probability

-- Here `x` is never used, since it is sampled in the lazy monad.
-- Therefore this does NOT test mcmc.
prior = do
    x <- normal 0.0 1.0
    let loggers = ["x" %=% x]
    return loggers

model = sample $ prior

main = mcmc model
