import           Probability

-- Note that `x` is always used even though it isn't observed.
-- That is because it is a return value in the strict monad.
-- Therefore this DOES test mcmc.
model = do
    x <- sample $ normal 0.0 1.0
    let loggers = ["x" %=% x]
    return loggers

main = mcmc model
