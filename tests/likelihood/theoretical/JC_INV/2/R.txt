require("gmp", quietly=TRUE, warn.conflicts=FALSE)
require("Rmpfr", quietly=TRUE, warn.conflicts=FALSE)
t = mpfr("1.0",prec=256)
p = mpfr("0.90",prec=256)
q1 = mpfr("0.25", prec=256)
q3 = mpfr("0.75", prec=256)
JC = q1 + q3*exp(-t/q3/(1-p))
INV = 1
print(log(q1*(1-p)*JC + q1*p*INV))
# -2.123543972393866071752114990328142038356657576437145099468896353521490466082845


